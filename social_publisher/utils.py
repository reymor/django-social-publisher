# coding=utf-8
from publisher import VideoProvider, ImageProvider, MessageProvider, ActionMessageProvider
from provider import LABEL_TYPE_IMAGE, LABEL_TYPE_VIDEO, LABEL_TYPE_MESSAGE, LABEL_TYPE_ACTION_MESSAGE


CONTENT_CLASS = {
    LABEL_TYPE_VIDEO: VideoProvider,
    LABEL_TYPE_IMAGE: ImageProvider,
    LABEL_TYPE_MESSAGE: MessageProvider,
    LABEL_TYPE_ACTION_MESSAGE: ActionMessageProvider
}


def social_networks_by_user(user, provider_class=None):
    from django.core.exceptions import ImproperlyConfigured
    from django.contrib.auth.models import User
    from django.contrib.sites.models import Site
    from templatetags.publisher_tags import CONTENT_CLASS
    from models import SocialNetwork
    import provider

    if not isinstance(user, User):
        raise ImproperlyConfigured("The user must be an instance of django User")

    result = []
    for adapter in provider.registry.get_list():
        if not provider_class or issubclass(adapter, CONTENT_CLASS[provider_class] if isinstance(provider_class, str) else provider_class):
            social_network = SocialNetwork.objects.get(provider=adapter.id, enabled=True)
            current_social_app = social_network.social_apps.get(
                sites__id=Site.objects.get_current().id
            )

            if current_social_app.provider in [socialaccount.provider
                                               for socialaccount in user.socialaccount_set.all()]:
                result.append(social_network)
    return result
