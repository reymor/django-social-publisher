#coding=utf-8
from django import template
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from ..models import SocialNetwork
from ..utils import CONTENT_CLASS

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_social_networks_by_content(context, content):
    if content not in CONTENT_CLASS:
        return []

    publishers = context.get('socialpublisher', {'channels': []})
    channels = publishers.get('channels')
    results = []

    for channel in channels:
        if issubclass(channel, CONTENT_CLASS[content]):
            try:
                results.append(SocialNetwork.objects.get(provider=channel.id, enabled=True))
            except SocialNetwork.DoesNotExist:
                pass

    return results


@register.assignment_tag(takes_context=True)
def get_social_networks_by_content_linked_to(context, content, user, linked=True):
    if not user and not isinstance(user, User):
        return []

    results = []
    social_networks = get_social_networks_by_content(context, content)

    accounts = {}
    for account in user.socialaccount_set.all().iterator():
        accounts.setdefault(account.provider, [])

    for social_network in social_networks:
        if linked:
            if get_current_social_app(social_network).provider in accounts:
                results.append(social_network)
        else:
            social_app = get_current_social_app(social_network)
            if social_app.provider not in accounts:
                results.append({
                    'network': social_network,
                    'provider': social_app.provider
                })

    return results


@register.assignment_tag
def get_current_social_app(social_network):
    return social_network.social_apps.get(
        sites__id=Site.objects.get_current().id
    )
